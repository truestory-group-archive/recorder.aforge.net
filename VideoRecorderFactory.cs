﻿using Recorder.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.AForge.Net
{
    public class VideoRecorderFactory : IVideoRecorderFactory
    {
        private ISavePathService _savePathService;

        public VideoRecorderFactory(ISavePathService savePathService)
        {
            _savePathService = savePathService;
        }

        public IRecorder Create(IVideoDevice device)
        {
            return new VideoRecorder(device, _savePathService);
        }
    }
}
