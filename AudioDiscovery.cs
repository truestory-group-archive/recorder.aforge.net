﻿using Recorder.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Recorder.Core.Models;
using AForge.Video.DirectShow;

namespace Recorder.AForge.Net
{
    public class AudioDiscovery : IAudioDiscovery
    {
        public IEnumerable<IAudioDevice> GetDevices()
        {
            var devices = new FilterInfoCollection(FilterCategory.AudioInputDevice);
            var parsed = new List<AudioDevice>();
            foreach (FilterInfo d in devices)
            {
                parsed.Add(new AudioDevice
                {
                    DeviceId = d.MonikerString,
                    Name = d.Name
                });
            }

            return parsed;
        }
    }
}
