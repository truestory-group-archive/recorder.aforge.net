﻿using Recorder.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Recorder.Core;
using Recorder.Core.Models;
using AForge.Video.FFMPEG;
using AForge.Video.DirectShow;
using AForge.Video;
using System.Drawing;
using Recorder.Core.Exceptions;

namespace Recorder.AForge.Net
{
    public class VideoRecorder : IRecorder
    {
        /** Recorder Framework specific fields **/
        private IVideoDevice _device;

        /** AForge.Net specific fields **/
        private VideoCaptureDevice _captureDevice = null;
        private ISavePathService _savePathService = null;

        public VideoRecorder(IVideoDevice device, ISavePathService savePathService)
        {
            _device = device;
            _savePathService = savePathService;
        }

        private VideoFileWriter _writer;
        
        public void Start()
        {
            if (_captureDevice != null && _captureDevice.IsRunning)
                throw new AlreadyRecordingException();
            _captureDevice = GetDevice(_device);
            if (_captureDevice == null)
                throw new DeviceNotFoundException();

            var savePath = _savePathService.GetPath(_device, "avi");
            if (string.IsNullOrEmpty(savePath))
                throw new System.IO.FileNotFoundException();

            _writer = new VideoFileWriter();
            
            _captureDevice.VideoResolution = GetResolution();

            int h = _captureDevice.VideoResolution.FrameSize.Height;
            int w = _captureDevice.VideoResolution.FrameSize.Width;
            _writer.Open(savePath, w, h, _captureDevice.VideoResolution.AverageFrameRate, VideoCodec.Default, 5000000);
            
            // _captureDevice.VideoResolution.AverageFrameRate
            _captureDevice.NewFrame += new NewFrameEventHandler(NewFrameHandler);
            _captureDevice.Start();
        }

        private VideoCapabilities GetResolution()
        {
            long maxPixels = 0;
            VideoCapabilities capability = null;

            // First determine max pixel size
            foreach(var c in _captureDevice.VideoCapabilities)
            {
                if(maxPixels < c.FrameSize.Height * c.FrameSize.Width)
                {
                    maxPixels = c.FrameSize.Height * c.FrameSize.Width;
                    capability = c;
                }
            }

            // Then determine max framerate
            var maxFrameRate = 0;
            foreach(var c in _captureDevice.VideoCapabilities)
            {
                if (c.FrameSize.Height * c.FrameSize.Width != maxPixels)
                    continue;

                if(maxFrameRate < c.AverageFrameRate)
                {
                    capability = c;
                    maxFrameRate = c.AverageFrameRate;
                }
            }

            return capability;
        }

        private VideoCaptureDevice GetDevice(IVideoDevice device)
        {
            var devices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo d in devices)
                if (d.MonikerString.ToLower().Contains(device.DeviceId.ToLower()))
                    return new VideoCaptureDevice(d.MonikerString);
            return null;
        }

        void NewFrameHandler(object sender, NewFrameEventArgs eventArgs)
        {
            try {
                using (var video = (Bitmap)eventArgs.Frame.Clone()) { 
                    _writer.WriteVideoFrame(video);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("[Exception] {0}", e.Message));
            }
        }

        public RecordingStatus Status()
        {
            if (_captureDevice != null && _captureDevice.IsRunning)
                return RecordingStatus.Recording;
            
            return RecordingStatus.Ready;
        }

        public void Stop()
        {
            if (_captureDevice == null || !_captureDevice.IsRunning)
                throw new NotRecordingException();

            _captureDevice.Stop();
            _writer.Close();
        }
    }
}
