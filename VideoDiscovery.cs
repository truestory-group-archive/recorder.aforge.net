﻿using Recorder.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Recorder.Core.Models;
using AForge.Video.DirectShow;

namespace Recorder.AForge.Net
{
    public class VideoDiscovery : IVideoDiscovery
    {
        public IEnumerable<IVideoDevice> GetDevices()
        {
            var devices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            var parsed = new List<VideoDevice>();
            foreach(FilterInfo d in devices)
            {
                parsed.Add(new VideoDevice
                {
                    DeviceId = d.MonikerString,
                    Name = d.Name
                });
            }

            return parsed;
        }
    }
}
